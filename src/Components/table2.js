import React, { Component } from 'react';

class Table2Item extends Component {
  changeStatus(project){
    this.props.project;
    this.setState({project:{
      id:this.props.project.id,
      title:this.props.project.title,
      category:this.props.project.category,
      status:this.refs.status.checked
    }},function () {
      this.props.changeStatus(this.state.project)
    })
  }
  render() {
    // console.log(this.props.project)
    return (
      <tr>
        <td>{this.props.project.id}</td>
        <td>{this.props.project.title}</td>
        <td>{this.props.project.category}</td>
        <td><input type="button" ref="status" value="Del" onClick={this.changeStatus.bind(this)} /></td>
      </tr>
     
    );
  }
}

export default Table2Item;
