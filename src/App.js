import React, { Component } from 'react';
import ProjectItem from './Components/projectItem';
import Table2Item from './Components/table2';
import _ from'lodash';
import './App.css';

class App extends Component {
  constructor(){
    super();
    this.state={
      projects:[{
        id:1,
        title:"Demo",
        category:"Interview",
        status:false
      },{
        id:2,
        title:"Demo1",
        category:"Interview1",
        status:false
      },{
        id:3,
        title:"Demo2",
        category:"Interview2",
        status:false
      }],
      newProjects:[]
    }
  }
  handleChangeStatus(project){
    let index = _.findIndex(this.state.projects, function(proj) { return proj.id == project.id });
    this.state.projects[index].status=project.status;
    let index1 = _.findIndex(this.state.newProjects, function(proj) { return proj.id == project.id });
    if(index1<0){
      this.state.newProjects.push(project)
    }else{
      this.state.newProjects.splice(index1,1)
    }
    this.forceUpdate();
  }
  render() {
    let uncheckedProjectItems,checkedProjectItems;
    if(this.state.projects){
      uncheckedProjectItems=this.state.projects.map(project=>{
        
          return(
            <ProjectItem key={project.id} project={project} changeStatus={this.handleChangeStatus.bind(this)}/>
          )
        
      })
      checkedProjectItems=this.state.newProjects.map(project=>{
        
          return(
            <Table2Item key={project.id} project={project} changeStatus={this.handleChangeStatus.bind(this)}/>
          )
        
      })
    }
    return (
      <div className="project">
        <table>
          <thead>
            <tr><td>Id</td><td>Title</td><td>Category</td><td></td></tr>
          </thead>
          <tbody>{uncheckedProjectItems}</tbody>
        </table>
        <table>
          <thead>
            <tr><td>Id</td><td>Title</td><td>Category</td><td></td></tr>
          </thead>
          <tbody>{this.state.newProjects.length>0?(
            checkedProjectItems
          ):(
            <tr><td colSpan="4">No Items</td></tr>
          )}
            {}
          </tbody>
        </table>
        
      </div>
    );
  }
}

export default App;
